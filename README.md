<h1 align="center" style="margin: 30px 0 30px; font-weight: bold;">UBDK企业级快速开发脚手架</h1>
<p align="center">
 <img src="https://img.shields.io/badge/universal--business--dev--kit-v1.0.0-brightgreen">
 <img src="https://img.shields.io/badge/Spring%20Cloud-2021.0.1-blue.svg" alt="Coverage Status">
 <img src="https://img.shields.io/badge/Spring%20Cloud%20Alibaba-2021.0.1.0-yellow.svg" alt="Coverage Status">
 <img src="https://img.shields.io/badge/Spring%20Boot-2.6.3-blue.svg" alt="Downloads">
</p>

<p align="center">
   <img src="https://gitee.com/jacklinnnn/universal-business-dev-kit/raw/master/ubdk.png">
</p>

##  🌈项目简介
>  `universal-business-dev-kit`简称是`UBDK` ，项目的初衷定位是一站式企业级开源技术解决方案工具堡垒，主要提供了消息中间件、缓存、代码生成、excel导入导出高度封装、分布式定时任务、多线程异步、短信服务等快速集成能力。基于`universal-business-dev-kit`可以不用关注技术细节即可拥有企业级开发常用的工具箱，从而快速搭建高并发高可用基于微服务的分布式架构。

## ✔模块介绍
```
universal-business-dev-kit
├── universal--business-dev-kit-components - 公共组件模块
    └── components-excel - excel导入导出封装
    └── components-mq - 消息中间件
    └── components-cache - 缓存
└── universal-business-dev-kit-core - 核心模块
└── universal-business-dev-kit-generator - 代码生成模块
└── universal-business-dev-kit-model - model层
└── universal-business-dev-kit-parent - parent层
└── universal-business-dev-kit-service - service业务层
└── universal-business-dev-kit-starter - starter启动层
```

## 👨‍关于作者

3年IT互联网Java开发工程师，曾开发过大型电商平台，熟悉SpringBoot、微服务、Spring Cloud Alibab、高并发。
