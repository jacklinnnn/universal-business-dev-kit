package com.universal.constants;

/**
 * 缓存有效期常量：单位s
 *
 * @author: austin
 * @since: 2023/2/2 11:22
 */
public class CacheExpireConst {

    //---------------- 缓存有效期：单位s（long类型）START ----------------

    public static final long LONG_1MIN = 60;
    public static final long LONG_10MIN = 10 * 60;
    public static final long LONG_30MIN = 30 * 60;

    /**
     * 3600 seconds = 1 hour
     * 24*3600(86400) seconds = 1 day
     */
    public static final long LONG_1HOUR = 60 * 60;
    public static final long LONG_2HOUR = 2 * LONG_1HOUR;
    public static final long LONG_6HOUR = 6 * LONG_1HOUR;
    public static final long LONG_24HOUR = 24 * LONG_1HOUR;


    public static final long LONG_1DAY = LONG_1HOUR * 24;
    public static final long LONG_1WEEK = LONG_1HOUR * 24 * 7;

    //----------------- 缓存有效期：单位s （long类型）END -----------------


    //---------------- 缓存有效期：单位s（int类型）START ----------------

    public static final int INT_1MIN = 60;
    public static final int INT_10MIN = 10 * 60;
    public static final int INT_30MIN = 30 * 60;

    /**
     * 3600 seconds = 1 hour
     * 24*3600(86400) seconds = 1 day
     */
    public static final int INT_1HOUR = 60 * 60;
    public static final int INT_2HOUR = 2 * INT_1HOUR;
    public static final int INT_6HOUR = 6 * INT_1HOUR;
    public static final int INT_24HOUR = 24 * INT_1HOUR;


    public static final int INT_1DAY = INT_1HOUR * 24;
    public static final int INT_1WEEK = INT_1HOUR * 24 * 7;

    //----------------- 缓存有效期：单位s （Long类型）END -----------------


}
