package com.universal.jedis;

/**
 * JedisBaseEnum
 *
 * @author: austin
 * @since: 2023/1/6 15:21
 */
public enum JedisBaseEnum {

    DB0,
    DB1,
    DB2,
    DB3,
    DB4,
    DB5,
    DB6,
    DB7,
    DB8,
    DB9,
    DB10,
    DB11;

    private JedisBaseEnum() {
    }
}
