package com.universal.jedis;

/**
 * JedisModelUtils
 *
 * @author: austin
 * @since: 2023/1/6 15:44
 */
public class JedisModelUtils extends JedisBase {

    private static JedisModelUtils instance;

    public static JedisModelUtils getInstance() {
        if (instance == null) {
            Class var0 = JedisModelUtils.class;
            synchronized(JedisModelUtils.class) {
                if (instance == null) {
                    instance = new JedisModelUtils();
                }
            }
        }

        return instance;
    }

    public JedisModelUtils() {
        super.setJedisEnum(JedisBaseEnum.DB1);
        super.setKeyPrefix("");
    }

    public <T> T getClassModel(Class<T> resultClass, String id) {
        String key = resultClass.getName() + "_" + id;
        return (T) super.getObject(key);
    }

    public void setClassModel(Object obj, String id) {
        String key = obj.getClass().getName() + "_" + id;
        super.setObject(key, obj, 3600);
    }
}
