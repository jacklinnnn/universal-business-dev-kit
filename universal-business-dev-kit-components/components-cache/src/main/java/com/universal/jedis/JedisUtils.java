package com.universal.jedis;

/**
 * Jedis Cache 工具类
 *
 * @author: austin
 * @since: 2023/1/6 15:42
 */
public class JedisUtils extends JedisBase {

    private static JedisUtils instance;

    private JedisUtils() {
        super.setJedisEnum(JedisBaseEnum.DB0);
        super.setKeyPrefix("common-cache:");
    }

    public static JedisUtils getInstance() {
        if (instance == null) {
            synchronized (JedisUtils.class) {
                instance = new JedisUtils();
            }
        }
        return instance;
    }
}
