package com.universal.redis;

import cn.hutool.core.bean.BeanUtil;
import com.google.common.base.Joiner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * RedisUtils工具类
 *
 * @author: austin
 * @since: 2023/2/2 12:26
 */
@Component
public class RedisUtils {

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private Environment env;

    private String keyPrefix;

    public String getKeyPrefix() {
        return keyPrefix;
    }

    @PostConstruct
    private void initKeyPrefix() {
        keyPrefix = env.getProperty("redis.key.prefix");
    }

    // -------------------------------- 公共方法 --------------------------------

    /**
     * key合并
     *
     * @param sources
     */
    public static String keyMerge(String... sources) {
        return Joiner.on(":").skipNulls().join(sources);
    }

    public String getNewKey(String key) {
        return keyPrefix + key;
    }

    /**
     * 给一个指定的 key 值附加过期时间
     *
     * @param key
     * @param time
     */
    public boolean expire(String key, long time) {
        key = keyPrefix + key;
        return redisTemplate.expire(key, time, TimeUnit.SECONDS);
    }

    /**
     * 根据key 获取过期时间
     *
     * @param key
     */
    public long getTime(String key) {
        key = keyPrefix + key;
        return redisTemplate.getExpire(key, TimeUnit.SECONDS);
    }

    /**
     * 根据key 获取过期时间
     *
     * @param key
     */
    public boolean hasKey(String key) {
        key = keyPrefix + key;
        return redisTemplate.hasKey(key);
    }

    /**
     * 移除指定key 的过期时间
     *
     * @param key
     */
    public boolean persist(String key) {
        key = keyPrefix + key;
        return redisTemplate.boundValueOps(key).persist();
    }

    // -------------------------------- String类型 --------------------------------

    /**
     * 根据key获取值
     *
     * @param key 键
     * @return 值
     */
    public Object get(String key) {
        key = keyPrefix + key;
        return key == null ? null : redisTemplate.opsForValue().get(key);
    }

    /**
     * 根据key获取值
     *
     * @param key 键
     * @return 值
     */
    public <T> T get(String key, Class<T> resultClass) {
        key = keyPrefix + key;
        return key == null ? null : BeanUtil.toBean(redisTemplate.opsForValue().get(key), resultClass);
    }

    /**
     * 将值放入缓存
     *
     * @param key   键
     * @param value 值
     * @return true成功 false 失败
     */
    public void set(String key, String value) {
        key = keyPrefix + key;
        redisTemplate.opsForValue().set(key, value);
    }

    /**
     * 将值放入缓存并设置时间
     *
     * @param key   键
     * @param value 值
     * @param time  时间(秒) -1为无期限
     * @return true成功 false 失败
     */
    public void set(String key, Object value, long time) {
        key = keyPrefix + key;
        if (time > 0) {
            redisTemplate.opsForValue().set(key, value, time, TimeUnit.SECONDS);
        } else {
            redisTemplate.opsForValue().set(key, value);
        }
    }

    /**
     * 将指放入缓存，根据时间单位设置过期时间
     *
     * @param key
     * @param value
     * @param time
     * @param timeUnit
     */
    public void set(String key, Object value, long time, TimeUnit timeUnit) {
        key = keyPrefix + key;
        redisTemplate.opsForValue().set(key, value, time, timeUnit);
    }

    /**
     * 批量添加 key (重复的键会覆盖)
     *
     * @param keyAndValue
     */
    public void batchSet(Map<String, String> keyAndValue) {
        redisTemplate.opsForValue().multiSet(keyAndValue);
    }

    /**
     * 批量添加 key-value 只有在键不存在时,才添加
     * map 中只要有一个key存在,则全部不添加
     *
     * @param keyAndValue
     */
    public void batchSetIfAbsent(Map<String, String> keyAndValue) {
        redisTemplate.opsForValue().multiSetIfAbsent(keyAndValue);
    }

    /**
     * 批量获取 key (重复的键会覆盖)
     *
     * @param keySet
     */
    public List<String> batchGet(Set<String> keySet) {
        return redisTemplate.opsForValue().multiGet(keySet);
    }

    /**
     * 批量添加 key (重复的键会覆盖)
     *
     * @param keyAndValue
     */
    public void batchSetObject(Map<String, Object> keyAndValue) {
        redisTemplate.opsForValue().multiSet(keyAndValue);
    }

    /**
     * 批量获取 key (重复的键会覆盖)
     *
     * @param keySet
     * @param parallelStream 并行执行流
     */
    public List<Object> batchGetWithKeyPrefix(Set<String> keySet, boolean parallelStream) {
        if (keySet.isEmpty()) {
            return null;
        }
        Set<String> newSet = new HashSet<>();
        if (parallelStream) {
            keySet.parallelStream().forEach(k -> newSet.add(keyPrefix + k));
        } else {
            keySet.stream().forEach(k -> newSet.add(keyPrefix + k));
        }
        return redisTemplate.opsForValue().multiGet(newSet);
    }

    /**
     * 批量添加 key (重复的键会覆盖)
     *
     * @param keyAndValue
     * @param parallelStream 并行执行流
     */
    public void batchSetObjectWithKeyPrefix(Map<String, Object> keyAndValue, boolean parallelStream) {
        if (keyAndValue.isEmpty()) {
            return;
        }
        Map<String, Object> newMap = new HashMap<>();
        Set<String> keys = keyAndValue.keySet();
        if (parallelStream) {
            keys.parallelStream().forEach(k -> newMap.put(keyPrefix + k, keyAndValue.get(k)));
        } else {
            keys.stream().forEach(k -> newMap.put(keyPrefix + k, keyAndValue.get(k)));
        }
        redisTemplate.opsForValue().multiSet(newMap);
    }

    /**
     * 批量添加 key-value 只有在键不存在时,才添加
     * map 中只要有一个key存在,则全部不添加
     *
     * @param keyAndValue
     */
    public void batchSetIfAbsentObject(Map<String, Object> keyAndValue) {
        redisTemplate.opsForValue().multiSetIfAbsent(keyAndValue);
    }

    /**
     * 批量获取 key (重复的键会覆盖)
     *
     * @param keySet
     */
    public List<Object> batchGetObject(Set<String> keySet) {
        return redisTemplate.opsForValue().multiGet(keySet);
    }

    /**
     * 对一个 key-value 的值进行加减操作,
     * 如果该 key 不存在 将创建一个key 并赋值该 number
     * 如果 key 存在,但 value 不是长整型 ,将报错
     *
     * @param key
     * @param number
     */
    public Long increment(String key, long number) {
        key = keyPrefix + key;
        return redisTemplate.opsForValue().increment(key, number);
    }

    /**
     * 对一个 key-value 的值进行加减操作,
     * 如果该 key 不存在 将创建一个key 并赋值该 number
     * 如果 key 存在,但 value 不是 纯数字 ,将报错
     *
     * @param key
     * @param number
     */
    public Double increment(String key, double number) {
        key = keyPrefix + key;
        return redisTemplate.opsForValue().increment(key, number);
    }

    // --------------------------------  set类型 --------------------------------

    /**
     * 将数据放入set缓存
     *
     * @param key 键
     */
    public void sSet(String key, Object value) {
        key = keyPrefix + key;
        redisTemplate.opsForSet().add(key, value);
    }

    /**
     * 获取变量中的值
     *
     * @param key 键
     */
    public Set<Object> members(String key) {
        key = keyPrefix + key;
        return redisTemplate.opsForSet().members(key);
    }

    /**
     * 随机获取变量中指定个数的元素
     *
     * @param key   键
     * @param count 值
     */
    public void randomMembers(String key, long count) {
        key = keyPrefix + key;
        redisTemplate.opsForSet().randomMembers(key, count);
    }

    /**
     * 随机获取变量中的元素
     *
     * @param key 键
     */
    public Object randomMember(String key) {
        key = keyPrefix + key;
        return redisTemplate.opsForSet().randomMember(key);
    }

    /**
     * 弹出变量中的元素
     *
     * @param key 键
     */
    public Object pop(String key) {
        key = keyPrefix + key;
        return redisTemplate.opsForSet().pop("setValue");
    }

    /**
     * 获取变量中值的长度
     *
     * @param key 键
     */
    public long size(String key) {
        key = keyPrefix + key;
        return redisTemplate.opsForSet().size(key);
    }

    /**
     * 根据value从一个set中查询,是否存在
     *
     * @param key   键
     * @param value 值
     * @return true 存在 false不存在
     */
    public boolean sHasKey(String key, Object value) {
        key = keyPrefix + key;
        return redisTemplate.opsForSet().isMember(key, value);
    }

    /**
     * 检查给定的元素是否在变量中。
     *
     * @param key 键
     * @param obj 元素对象
     */
    public boolean isMember(String key, Object obj) {
        key = keyPrefix + key;
        return redisTemplate.opsForSet().isMember(key, obj);
    }

    /**
     * 转移变量的元素值到目的变量。
     *
     * @param key     键
     * @param value   元素对象
     * @param destKey 元素对象
     */
    public boolean move(String key, String value, String destKey) {
        key = keyPrefix + key;
        return redisTemplate.opsForSet().move(key, value, destKey);
    }

    /**
     * 批量移除set缓存中元素
     *
     * @param key    键
     * @param values 值
     */
    public void remove(String key, Object... values) {
        key = keyPrefix + key;
        redisTemplate.opsForSet().remove(key, values);
    }

    /**
     * 通过给定的key求2个set变量的差值
     *
     * @param key     键
     * @param destKey 键
     * @return
     */
    public Set<Set> difference(String key, String destKey) {
        key = keyPrefix + key;
        return redisTemplate.opsForSet().difference(key, destKey);
    }


    // -------------------------------- hash类型 --------------------------------

    /**
     * 加入缓存
     *
     * @param key 键
     * @param map 键
     * @return
     */
    public void add(String key, Map<String, String> map) {
        key = keyPrefix + key;
        redisTemplate.opsForHash().putAll(key, map);
    }

    /**
     * 加入缓存
     *
     * @param key 键
     * @param map 键
     */
    public void addObjectMap(String key, Map<String, Object> map) {
        key = keyPrefix + key;
        redisTemplate.opsForHash().putAll(key, map);
    }

    /**
     * 获取 key 下的 所有  hashKey 和 value
     *
     * @param key 键
     */
    public Map<Object, Object> getHashEntries(String key) {
        key = keyPrefix + key;
        return redisTemplate.opsForHash().entries(key);
    }

    /**
     * 验证指定 key 下 有没有指定的 hashKey
     *
     * @param key
     * @param hashKey
     */
    public boolean hashKey(String key, String hashKey) {
        key = keyPrefix + key;
        return redisTemplate.opsForHash().hasKey(key, hashKey);
    }

    /**
     * 获取指定key的值string
     *
     * @param key  键
     * @param key2 键
     */
    public String getMapString(String key, String key2) {
        key = keyPrefix + key;
        return (String) redisTemplate.opsForHash().get(key, key2);
    }

    /**
     * 获取指定的值Int
     *
     * @param key  键
     * @param key2 键
     */
    public Integer getMapInt(String key, String key2) {
        key = keyPrefix + key;
        key2 = keyPrefix + key;
        return (Integer) redisTemplate.opsForHash().get(key2, key2);
    }

    /**
     * 弹出元素并删除
     *
     * @param key 键
     */
    public String popValue(String key) {
        key = keyPrefix + key;
        return redisTemplate.opsForSet().pop(key).toString();
    }

    /**
     * 删除指定 hash 的 HashKey
     *
     * @param key
     * @param hashKeys
     * @return 删除成功的 数量
     */
    public Long delete(String key, String... hashKeys) {
        key = keyPrefix + key;
        return redisTemplate.opsForHash().delete(key, hashKeys);
    }

    public Long deleteHashKey(String key) {
        String newKey = keyPrefix + key;
        Set<String> hashKeys = redisTemplate.opsForHash().keys(newKey);
        if (hashKeys == null || hashKeys.isEmpty()) {
            return 0L;
        }
        String[] hkeys = hashKeys.toArray(new String[hashKeys.size()]);
        return redisTemplate.opsForHash().delete(newKey, hkeys);
    }

    /**
     * 给指定 hash 的 hashKey 做增减操作
     *
     * @param key
     * @param hashKey
     * @param number
     */
    public Long increment(String key, String hashKey, long number) {
        key = keyPrefix + key;
        return redisTemplate.opsForHash().increment(key, hashKey, number);
    }

    /**
     * 给指定 hash 的 hashKey 做增减操作
     *
     * @param key
     * @param hashKey
     * @param number
     */
    public Double increment(String key, String hashKey, Double number) {
        key = keyPrefix + key;
        return redisTemplate.opsForHash().increment(key, hashKey, number);
    }

    /**
     * 获取 key 下的 所有 hashKey 字段
     *
     * @param key
     */
    public Set<Object> hashKeys(String key) {
        key = keyPrefix + key;
        return redisTemplate.opsForHash().keys(key);
    }

    /**
     * 获取指定 hash 下面的 键值对 数量
     *
     * @param key
     */
    public Long hashSize(String key) {
        key = keyPrefix + key;
        return redisTemplate.opsForHash().size(key);
    }

    // -------------------------------- list类型 --------------------------------

    /**
     * 在变量左边添加元素值
     *
     * @param key
     * @param value
     */
    public void leftPush(String key, Object value) {
        key = keyPrefix + key;
        redisTemplate.opsForList().leftPush(key, value);
    }

    /**
     * 获取集合指定位置的值。
     *
     * @param key
     * @param index
     */
    public Object index(String key, long index) {
        key = keyPrefix + key;
        return redisTemplate.opsForList().index(key, index);
    }


    /**
     * 获取指定区间的值。
     *
     * @param key
     * @param start
     * @param end
     */
    public <T> List<T> range(String key, long start, long end) {
        key = keyPrefix + key;
        return redisTemplate.opsForList().range(key, start, end);
    }

    /**
     * 把最后一个参数值放到指定集合的第一个出现中间参数的前面，
     * 如果中间参数值存在的话。
     *
     * @param key
     * @param pivot
     * @param value
     */
    public void leftPush(String key, String pivot, String value) {
        key = keyPrefix + key;
        redisTemplate.opsForList().leftPush(key, pivot, value);
    }

    /**
     * 向左边批量添加参数元素。
     *
     * @param key
     * @param
     */
    public void leftPushAll(String key, long time, String... values) {
        key = keyPrefix + key;
        redisTemplate.opsForList().leftPushAll(key, values);
        expire(key, time);
    }

    /**
     * 向左边批量添加参数元素。
     *
     * @param key
     * @param values
     */
    public <T> void leftPushAll(String key, List<T> values, long time) {
        key = keyPrefix + key;
        redisTemplate.opsForList().leftPushAll(key, values);
        expire(key, time);
    }

    /**
     * 向集合最右边添加元素。
     *
     * @param key
     * @param value
     */
    public void leftPushAll(String key, String value) {
        key = keyPrefix + key;
        redisTemplate.opsForList().rightPush(key, value);
    }

    /**
     * 向左边批量添加参数元素。
     *
     * @param key
     * @param values
     */
    public void rightPushAll(String key, String... values) {
        key = keyPrefix + key;
        redisTemplate.opsForList().rightPushAll(key, values);
    }

    /**
     * 向已存在的集合中添加元素。
     *
     * @param key
     * @param value
     */
    public void rightPushIfPresent(String key, Object value) {
        key = keyPrefix + key;
        redisTemplate.opsForList().rightPushIfPresent(key, value);
    }

    /**
     * 向已存在的集合中添加元素。
     *
     * @param key
     */
    public long listLength(String key) {
        key = keyPrefix + key;
        return redisTemplate.opsForList().size(key);
    }

    /**
     * 移除集合中的左边第一个元素。
     *
     * @param key
     */
    public void leftPop(String key) {
        key = keyPrefix + key;
        redisTemplate.opsForList().leftPop(key);
    }

    /**
     * 移除集合中左边的元素在等待的时间里，如果超过等待的时间仍没有元素则退出。
     *
     * @param key
     */
    public void leftPop(String key, long timeout, TimeUnit unit) {
        key = keyPrefix + key;
        redisTemplate.opsForList().leftPop(key, timeout, unit);
    }

    /**
     * 移除集合中右边的元素。
     *
     * @param key
     */
    public void rightPop(String key) {
        key = keyPrefix + key;
        redisTemplate.opsForList().rightPop(key);
    }

    /**
     * 移除集合中右边的元素在等待的时间里，如果超过等待的时间仍没有元素则退出。
     *
     * @param key
     */
    public void rightPop(String key, long timeout, TimeUnit unit) {
        key = keyPrefix + key;
        redisTemplate.opsForList().rightPop(key, timeout, unit);
    }

    /**
     * 获取当前key在redis中的有效期
     *
     * @param timeUnit
     * @param key
     */
    public Long getExpirySeconds(String key, TimeUnit timeUnit) {
        key = keyPrefix + key;
        return redisTemplate.opsForValue().getOperations().getExpire(key, timeUnit);
    }

    /**
     * 计数器，每调用一次自动加1
     *
     * @param key
     * @param expiry
     * @param timeUnit
     */
    public Integer incrementWithExpiry(String key, long expiry, TimeUnit timeUnit) {
        key = keyPrefix + key;
        Long increment = redisTemplate.opsForValue().increment(key, 1);
        redisTemplate.expire(key, expiry, timeUnit);
        return increment.intValue();
    }

    /**
     * 根据指定key删除redis中的值
     *
     * @param key
     */
    public Boolean deleteObject(String key) {
        key = keyPrefix + key;
        return redisTemplate.delete(key);
    }

    /**
     * 根据key关键字查询key集合
     *
     * @param keyword
     */
    public Set<String> keys(String keyword) {
        return redisTemplate.keys(keyword);
    }

    /**
     * 根据指定key删除redis中的值（不拼接keyPrefix）
     *
     * @param key
     */
    public Boolean deleteObjectJustKey(String key) {
        return redisTemplate.delete(key);
    }

    /**
     * SetIfAbsent
     *
     * @param key
     * @param expiry
     * @param timeUnit
     */
    public boolean setIfAbsent(String key, long expiry, TimeUnit timeUnit) {
        key = keyPrefix + key;
        return redisTemplate.opsForValue().setIfAbsent(key, 1, expiry, timeUnit);
    }

    // --------------------------------  zSet --------------------------------

    public Long count(String key, double min, double max) {
        key = keyPrefix + key;
        return redisTemplate.opsForZSet().count(key, min, max);
    }

    public Boolean zAdd(String key, Object value, double score) {
        key = keyPrefix + key;
        return redisTemplate.opsForZSet().add(key, value, score);
    }

    public Long zRemByScore(String key, double scoreFrom, double scoreTo) {
        key = keyPrefix + key;
        return redisTemplate.opsForZSet().removeRangeByScore(key, scoreFrom, scoreTo);
    }

    public Boolean zAddIfAbsent(String key, Object value, double score) {
        key = keyPrefix + key;
        return redisTemplate.opsForZSet().addIfAbsent(key, value, score);
    }

    public <T> T execute(String script, Class<T> returnType, List keys, Object... args) {
        if (!CollectionUtils.isEmpty(keys)) {
            keys = (List) keys.stream().map(k -> getNewKey(String.valueOf(k)))
                    .collect(Collectors.toList());
        }
        return (T) redisTemplate.execute(new DefaultRedisScript(script, returnType), keys, args);
    }
}
