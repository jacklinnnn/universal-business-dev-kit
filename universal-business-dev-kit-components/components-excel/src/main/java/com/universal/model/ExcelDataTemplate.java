package com.universal.model;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ExcelDataTemplate {

    /**
     * 网点编码
     */
    @ExcelProperty(value = "网点编码")
    private String code;

    /**
     * 网点名称
     */
    @ExcelProperty(value = "网点名称")
    private String name;

    /**
     * 状态
     */
    @ExcelProperty(value = "状态")
    private Integer status;
}
