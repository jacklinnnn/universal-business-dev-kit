package com.universal.base.api;

import com.universal.base.entity.CollectionsEntity;
import com.universal.base.entity.PageEntity;

import java.io.Serializable;
import java.util.List;

/**
 * 通用方法
 *
 * @author austin
 * @data 2022/11/22 11:40
 */
public interface IBaseApi<T> {

    T getById(Serializable id);

    long count(T entity);

    T getOne(T t);

    List<T> list(CollectionsEntity<T> collectionsEntity);

    PageEntity<T> page(PageEntity<T> pageEntity);

    boolean saveBatch(List<T> list);

    boolean saveOrUpdateBatch(List<T> list);

    boolean insert(T t);

    T save(T t);

    T saveOrUpdate(T t);

    boolean removeById(Serializable id);
}
