package com.universal.base.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Custom BaseEntity 基础公共字段
 *
 * @author austin
 * @data 2022/11/14 10:08
 */
@Data
public class BaseEntity implements Serializable {

    private static final long serialVersionUID = -4890180601007468000L;

    @TableField("id")
    @TableId(type = IdType.ASSIGN_ID)
    private String id;

    @TableField(value = "create_time", fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @TableField("create_by")
    private String createBy;

    @TableField(value = "update_time", fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    @TableField("update_by")
    private String updateBy;

    @TableLogic
    @TableField(select = false)
    private Integer deleted = 0;
}
