package com.universal.base.entity;

import com.google.common.collect.Lists;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * Custom CollectionsEntity
 *
 * @author austin
 * @data 2022/11/14 10:04
 */
@Data
@NoArgsConstructor
public class CollectionsEntity<T> implements Serializable {

    private static final long serialVersionUID = -5263631378171072539L;

    private T entity;

    /**
     * 多功能查询
     */
    private List<RuleEntity> ruleList;

    /**
     * 排序MAP ,ex: order by sort desc  ==  orderByString=-sort
     * 排序MAP ,ex: order by sort asc  == orderByString=+sort
     * 多个排序用,分隔
     */
    private String orderBy;

    /**
     * @param entity
     */
    public CollectionsEntity(T entity) {
        this.entity = entity;
    }

    /**
     * @param entity
     * @param orderBy
     */
    public CollectionsEntity(T entity, String orderBy) {
        this.entity = entity;
        this.orderBy = orderBy;
    }

    public void setRuleListObjects(RuleEntity... entitys) {
        ruleList = Lists.newArrayList(entitys);
    }
}
