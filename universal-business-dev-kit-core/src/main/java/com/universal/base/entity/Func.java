package com.universal.base.entity;

/**
 * 函数式声明
 *
 * @author: austin
 * @since: 2023/1/6 15:34
 */
public class Func {
    public Func() {
    }

    public interface Function7<TIn1, TIn2, TIn3, TIn4, TIn5, TIn6, TIn7, TRet> {
        TRet apply(TIn1 var1, TIn2 var2, TIn3 var3, TIn4 var4, TIn5 var5, TIn6 var6, TIn7 var7);
    }

    public interface Function6<TIn1, TIn2, TIn3, TIn4, TIn5, TIn6, TRet> {
        TRet apply(TIn1 var1, TIn2 var2, TIn3 var3, TIn4 var4, TIn5 var5, TIn6 var6);
    }

    public interface Function5<TIn1, TIn2, TIn3, TIn4, TIn5, TRet> {
        TRet apply(TIn1 var1, TIn2 var2, TIn3 var3, TIn4 var4, TIn5 var5);
    }

    public interface Function4<TIn1, TIn2, TIn3, TIn4, TRet> {
        TRet apply(TIn1 var1, TIn2 var2, TIn3 var3, TIn4 var4);
    }

    public interface Function3<TIn1, TIn2, TIn3, TRet> {
        TRet apply(TIn1 var1, TIn2 var2, TIn3 var3);
    }

    public interface Function2<TIn1, TIn2, TRet> {
        TRet apply(TIn1 var1, TIn2 var2);
    }

    public interface Function1<TIn1, TRet> {
        TRet apply(TIn1 var1);
    }

    public interface Function0<TRet> {
        TRet apply();
    }
}
