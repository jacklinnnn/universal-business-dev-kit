package com.universal.base.entity;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.common.collect.Lists;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * Custom PageEntity
 *
 * @author austin
 * @data 2022/11/14 10:05
 */
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class PageEntity<T> extends Page<T> implements IPage<T> {


    private static final long serialVersionUID = -5426529937929894701L;

    private T entity;

    /**
     * 排序MAP ,ex: order by sort desc  ==  orderByString=-sort
     * 排序MAP ,ex: order by sort asc  ==  orderByString=+sort
     * 多个排序用,分隔
     */
    private String orderBy;

    /**
     * 多功能查询
     */
    private List<RuleEntity> ruleList;


    /**
     * @param current
     * @param size
     */
    public PageEntity(long current, long size) {
        super(current, size, 0L);
    }

    /**
     * @param current
     * @param size
     * @param isSearchCount
     */
    public PageEntity(long current, long size, boolean isSearchCount) {
        super(current, size, isSearchCount);
    }

    /**
     * @param current
     * @param size
     * @param totola
     * @return
     */
    public PageEntity(long current, long size, long totola) {
        super(current, size, totola);
    }

    /**
     * @param current
     * @param size
     * @param totola
     * @param isSearchCount
     */
    public PageEntity(long current, long size, long totola, boolean isSearchCount) {
        super(current, size, totola, isSearchCount);
    }

    /**
     * @param page
     */
    public PageEntity(IPage page) {
        super(page.getCurrent(), page.getSize(), page.getTotal());
    }

    public T getEntity() {
        return entity;
    }

    public void setEntity(T entity) {
        this.entity = entity;
    }


    public String getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    public List<RuleEntity> getRuleList() {
        return ruleList;
    }

    public void setRuleList(List<RuleEntity> ruleList) {
        this.ruleList = ruleList;
    }

    public void setRuleListObjects(RuleEntity... entitys) {
        ruleList = Lists.newArrayList(entitys);
    }

    /**
     * 添加查询规则
     *
     * @param ruleEntity
     */
    public void addRuleEntity(RuleEntity ruleEntity) {

        if (CollectionUtils.isEmpty(this.ruleList)) {
            ruleList = Lists.newArrayList(ruleEntity);
        } else {
            this.ruleList.add(ruleEntity);
        }
    }
}
