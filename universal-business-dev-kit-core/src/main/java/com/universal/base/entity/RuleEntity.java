package com.universal.base.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * Custom RuleEntity
 *
 * @author austin
 * @data 2022/11/14 10:05
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RuleEntity implements Serializable {

    public final static String TYPE_EQ = "=";
    public final static String TYPE_NE = "!=";
    public final static String TYPE_LT = "<";
    public final static String TYPE_LE = "<=";
    public final static String TYPE_GT = ">";
    public final static String TYPE_GE = ">=";
    public final static String TYPE_IN = "in";
    public final static String TYPE_NOT_IN = "not in";
    public final static String TYPE_LIKE = "like";
    public final static String TYPE_ISNULL = "is null";
    public final static String TYPE_ISNOTNULL = "is not null";

    private static final long serialVersionUID = 1L;

    private String pro;
    private String type;
    private Object value;
}
