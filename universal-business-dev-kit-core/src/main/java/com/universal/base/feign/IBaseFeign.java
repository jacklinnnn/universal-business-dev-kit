package com.universal.base.feign;

import com.universal.base.entity.CollectionsEntity;
import com.universal.base.entity.PageEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.io.Serializable;
import java.util.List;

/**
 * Custom IBaseFeign
 *
 * @author austin
 * @data 2022/11/14 10:03
 */
public interface IBaseFeign<T> {

    @GetMapping(value = "get/{id}")
    T getById(@PathVariable("id") Serializable var1);

    @PostMapping(value = "count")
    long count(@RequestBody T var1);

    @PostMapping(value = "getOne")
    T getOne(@RequestBody T var1);

    @PostMapping(value = "list")
    List<T> list(@RequestBody T var1);

    @PostMapping(value = "listByCollectionEntity")
    List<T> listByCollectionEntity(@RequestBody CollectionsEntity<T> collectionsEntity);

    @PostMapping(value = "page")
    PageEntity<T> page(@RequestBody PageEntity<T> pageEntity);

    @PostMapping(value = "saveOrUpdateBatch")
    boolean saveOrUpdateBatch(@RequestBody List<T> list);

    @PostMapping(value = "saveBatch")
    boolean saveBatch(@RequestBody List<T> list);

    @PostMapping(value = "save")
    T save(@RequestBody T t);

    @PostMapping(value = "saveOrUpdate")
    T saveOrUpdate(@RequestBody T t);

    @DeleteMapping(value = "delete/{id}")
    boolean removeById(@PathVariable("id") Serializable id);

    @PostMapping(value = "updateById")
    boolean updateById(@RequestBody T var1);
}
