package com.universal.common.contants;

/**
 * 缓存常量类
 *
 * @author: austin
 * @date: 2022/11/13 21:45
 */
public class CacheConst {

    public static final String COMMON_CACHE_PREFIX = "universal:";

    public static final String SYS_CONFIG_KEY = "sys:config:";

    public static final String REDIS_LOCK_KEY = "lock:";

    //缓存有效期：一天
    public static final Long ALL_DETAIL_TIME_OUT = 864000L;
}
