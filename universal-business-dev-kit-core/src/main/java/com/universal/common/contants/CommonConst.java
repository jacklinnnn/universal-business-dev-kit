package com.universal.common.contants;

/**
 * 公共常量类
 *
 * @author: austin
 * @date: 2022/11/13 21:45
 */
public class CommonConst {

    public final static Integer I1 = 1;
    public final static Integer I2 = 2;
    public final static Integer I3 = 3;

    public final static String S1 = "1";
    public final static String S2 = "2";
    public final static String S3 = "3";
}
