package com.universal.common.dsPatterns.enums;

import lombok.Getter;

/**
 * 支付策略枚举
 */
@Getter
public enum PayStrategyEnum {

    ZFB("ZFB", "com.universal.core.designPatterns.factory.impl.AliPay"),
    WX("WX", "com.universal.core.designPatterns.factory.impl.WechatPay"),
    UNION("UNION", "com.universal.core.designPatterns.factory.impl.UnionPay");
    String type;
    String value;

    PayStrategyEnum(String type, String value) {
        this.type = type;
        this.value = value;
    }
}