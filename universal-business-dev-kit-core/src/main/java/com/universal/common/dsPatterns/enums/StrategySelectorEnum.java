package com.universal.common.dsPatterns.enums;

import lombok.Getter;

@Getter
public enum StrategySelectorEnum {

    strategyA(1,"strategyA"),
    strategyB(2,"strategyB"),
    strategyC(3,"strategyC");

    private Integer code;
    private String strategy;

    StrategySelectorEnum(Integer code, String strategy) {
        this.code = code;
        this.strategy = strategy;
    }
}
