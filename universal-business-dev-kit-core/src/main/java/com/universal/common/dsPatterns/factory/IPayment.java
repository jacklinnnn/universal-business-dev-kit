package com.universal.common.dsPatterns.factory;

public interface IPayment {

    /**
     * 支付
     *
     * @param paymentBody
     */
    Boolean pay(PaymentBody paymentBody);
}
