package com.universal.common.dsPatterns.factory;

import lombok.Data;

import java.util.Map;

@Data
public abstract class PaymentBody implements Map {

    /**
     * 支付方式
     */
    private String type;
}
