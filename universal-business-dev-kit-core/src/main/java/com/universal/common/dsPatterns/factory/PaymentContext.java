package com.universal.common.dsPatterns.factory;

import lombok.Data;

import javax.annotation.Resource;

@Data
public class PaymentContext {

    @Resource
    private IPayment paymentStrategy;

    public PaymentContext(IPayment paymentStrategy) {
        this.paymentStrategy = paymentStrategy;
    }

    public Boolean pay(PaymentBody paymentBody) {
        return this.paymentStrategy.pay(paymentBody);
    }
}
