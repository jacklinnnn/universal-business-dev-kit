package com.universal.common.dsPatterns.factory;

import cn.hutool.core.util.EnumUtil;
import cn.hutool.core.util.ReflectUtil;
import com.universal.common.dsPatterns.enums.PayStrategyEnum;
import org.springframework.stereotype.Component;

/**
 * Factory for payment methods
 */
@Component
public class PaymentFactory {

    public static IPayment getPayStrategy(String type) {
        // 1.通过枚举中的type获取对应的value
        String value = EnumUtil.getFieldBy(PayStrategyEnum::getValue, PayStrategyEnum::getType, type);
        // 2.使用反射机制创建对应的策略类
        IPayment payment = ReflectUtil.newInstance(value);
        return payment;
    }
}
