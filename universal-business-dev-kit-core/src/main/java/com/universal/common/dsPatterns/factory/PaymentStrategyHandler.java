package com.universal.common.dsPatterns.factory;

import cn.hutool.core.util.EnumUtil;
import com.universal.common.dsPatterns.enums.PayStrategyEnum;
import org.springframework.stereotype.Component;

@Component
public class PaymentStrategyHandler {

    public static Boolean pay(PaymentBody payBody) {
        if (!EnumUtil.contains(PayStrategyEnum.class, payBody.getType())) {
            throw new IllegalArgumentException("不支持的支付方式!");
        }
        // 1.获取支付策略对象
        IPayment payStrategy = PaymentFactory.getPayStrategy(payBody.getType());
        // 2.获取支付策略上下文
        PaymentContext paymentContext = new PaymentContext(payStrategy);
        // 3.进行支付
        return paymentContext.pay(payBody);
    }
}
