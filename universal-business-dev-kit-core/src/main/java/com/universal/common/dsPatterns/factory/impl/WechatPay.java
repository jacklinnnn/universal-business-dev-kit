package com.universal.common.dsPatterns.factory.impl;

import com.universal.common.dsPatterns.factory.IPayment;
import com.universal.common.dsPatterns.factory.PaymentBody;

public class WechatPay implements IPayment {
    @Override
    public Boolean pay(PaymentBody paymentBody) {
        System.out.println("微信支付...");
        return Boolean.TRUE;
    }
}
