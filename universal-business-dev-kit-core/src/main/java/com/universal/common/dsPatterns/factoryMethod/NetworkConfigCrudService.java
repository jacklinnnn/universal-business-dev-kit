package com.universal.common.dsPatterns.factoryMethod;

import com.universal.common.dsPatterns.factoryMethod.dto.NetworkConfigDTO;
import com.universal.common.dsPatterns.factoryMethod.vo.NetworkConfigVO;

public interface NetworkConfigCrudService {

    NetworkConfigVO getNetwork(NetworkConfigDTO networkConfigDTO);

}
