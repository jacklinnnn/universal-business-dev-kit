package com.universal.common.dsPatterns.factoryMethod;

import com.universal.common.dsPatterns.factoryMethod.dto.NetworkConfigDTO;

public interface NetworkConfigService {

    String getNetwork(NetworkConfigDTO networkConfigDTO);
}
