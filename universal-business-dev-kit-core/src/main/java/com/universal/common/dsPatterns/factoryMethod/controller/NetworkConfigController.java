package com.universal.common.dsPatterns.factoryMethod.controller;

import com.universal.base.resp.ApiResult;
import com.universal.common.dsPatterns.factoryMethod.NetworkConfigCrudService;
import com.universal.common.dsPatterns.factoryMethod.dto.NetworkConfigDTO;
import com.universal.common.dsPatterns.factoryMethod.factory.NetworkConfigFactoryService;
import com.universal.common.dsPatterns.factoryMethod.vo.NetworkConfigVO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@RequestMapping(value = "/networkConfig", produces = MediaType.APPLICATION_JSON_VALUE)
@RequiredArgsConstructor
public class NetworkConfigController {
    private final NetworkConfigFactoryService factoryService;

    @PostMapping(value = "/getNetworkDetails", produces = MediaType.APPLICATION_JSON_VALUE)
    public ApiResult<NetworkConfigVO> getNetworkDetails(@RequestBody NetworkConfigDTO networkConfigDTO) {
        NetworkConfigCrudService aService = factoryService.getSpecificService("A");
        NetworkConfigVO network = aService.getNetwork(networkConfigDTO);
        return ApiResult.success(network);
    }

}
