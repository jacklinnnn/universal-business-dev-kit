package com.universal.common.dsPatterns.factoryMethod.dto;

public class NetworkConfigDTO {

    /**
     * 当前页
     */
    private Integer current = 1;
    /**
     * 每页大小
     */
    private Integer pageSize = 10;
    /**
     * 大客户id(查询大客户配置网点时必传)
     */
    private String agentId;
    /**
     * 休息室编码/名称/站点编码/名称/区域(大洲|国家|省份|城市)名称
     */
    private String name;
    /**
     * 项目id(查询项目配置网点时必传)
     */
    private String projectId;
    /**
     * 语种(必传)
     */
    private String lg;
    /**
     * 状态
     */
    private Long status ;
}
