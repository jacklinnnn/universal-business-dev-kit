package com.universal.common.dsPatterns.factoryMethod.factory;

import com.universal.common.dsPatterns.factoryMethod.NetworkConfigCrudService;

public interface NetworkConfigFactoryService {

    /**
     * 获取指定的处理逻辑类
     *
     * @param productType
     * @return
     */
    NetworkConfigCrudService getSpecificService(String productType);
}
