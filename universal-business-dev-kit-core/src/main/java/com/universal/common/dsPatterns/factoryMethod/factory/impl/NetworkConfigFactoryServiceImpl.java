package com.universal.common.dsPatterns.factoryMethod.factory.impl;

import com.universal.common.dsPatterns.factoryMethod.impl.AServiceImpl;
import com.universal.common.dsPatterns.factoryMethod.impl.BServiceImpl;
import com.universal.common.dsPatterns.factoryMethod.impl.CServiceImpl;
import com.universal.common.dsPatterns.factoryMethod.impl.DServiceImpl;
import com.universal.common.dsPatterns.factoryMethod.NetworkConfigCrudService;
import com.universal.common.dsPatterns.factoryMethod.factory.NetworkConfigFactoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class NetworkConfigFactoryServiceImpl implements NetworkConfigFactoryService {

    private final AServiceImpl aService;
    private final BServiceImpl bService;
    private final CServiceImpl cService;
    private final DServiceImpl dService;

    @Override
    public NetworkConfigCrudService getSpecificService(String productType) {
        NetworkConfigCrudService networkConfigCrudService = null;
        switch (productType){
            case "A" :
                networkConfigCrudService = aService;
                break;
            case "B":
                networkConfigCrudService = bService;
                break;
            case "C":
                networkConfigCrudService = cService;
                break;
            case "D":
                networkConfigCrudService = dService;
                break;
        }
        return networkConfigCrudService;
    }
}
