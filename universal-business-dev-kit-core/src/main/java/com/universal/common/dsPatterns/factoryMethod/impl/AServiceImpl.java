package com.universal.common.dsPatterns.factoryMethod.impl;

import com.universal.common.dsPatterns.factoryMethod.NetworkConfigCrudService;
import com.universal.common.dsPatterns.factoryMethod.dto.NetworkConfigDTO;
import com.universal.common.dsPatterns.factoryMethod.vo.NetworkConfigVO;
import org.springframework.stereotype.Service;

@Service
public class AServiceImpl implements NetworkConfigCrudService {
    @Override
    public NetworkConfigVO getNetwork(NetworkConfigDTO networkConfigDTO) {
        return new NetworkConfigVO();
    }
}
