package com.universal.common.dsPatterns.factoryMethod.impl;

import com.universal.common.dsPatterns.factoryMethod.NetworkConfigService;
import com.universal.common.dsPatterns.factoryMethod.dto.NetworkConfigDTO;
import org.springframework.stereotype.Service;

@Service
public class NetworkConfigServiceImpl implements NetworkConfigService {

    @Override
    public String getNetwork(NetworkConfigDTO networkConfigDTO) {
        return new String();
    }
}
