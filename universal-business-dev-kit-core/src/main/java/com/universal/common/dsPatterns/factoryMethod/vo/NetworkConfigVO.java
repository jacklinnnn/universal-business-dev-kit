package com.universal.common.dsPatterns.factoryMethod.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NetworkConfigVO {

    /**
     * id
     */
    private String id;
    /**
     * 大洲名称
     */
    private String continentName;
    /**
     * 国家名称
     */
    private String countryName;
    /**
     * 城市名称
     */
    private String cityName;
    /**
     * 网点编码
     */
    private String code;
    /**
     * 网点名称
     */
    private String name;
    /**
     * 行政区域编码
     */
    private String areaCode;
    /**
     * 行政区域名称
     */
    private String areaName;
    /**
     * 站点编码
     */
    private String stationCode;
    /**
     * 站点名称
     */
    private String stationName;
    /**
     * 限制类型
     */
    private String type = "1";
    /**
     * 是否特殊网点(中台数据showApp为不显示) 0:否 1:是  是否展示 (必要判断 ) 0不展示1展示
     */
    private Long isSpecialNetwork;
}
