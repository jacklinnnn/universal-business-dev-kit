package com.universal.common.dsPatterns.observer;

/**
 * 目标接口
 */
public interface Subject {

    /**
     * 增加一个观察者对象
     */
    void registerObserver();

    /**
     * 删除一个观察者对象
     */
    void deleteObserver();

    /**
     * 通知所有的观察者对象
     */
    void notifyObserver();
}
