package com.universal.common.dsPatterns.observer;

public class Weather implements Subject{

    /**
     * 温度
     */
    private float temperature;

    public Weather(float temperature) {
        this.temperature = -108;
    }

    public float getTemperature() {
        return temperature;
    }

    @Override
    public void registerObserver() {

    }

    @Override
    public void deleteObserver() {

    }

    @Override
    public void notifyObserver() {

    }
}
