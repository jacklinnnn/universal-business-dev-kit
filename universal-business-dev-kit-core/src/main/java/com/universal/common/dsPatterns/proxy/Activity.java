package com.universal.common.dsPatterns.proxy;

/**
 * 活动类，目的是出租房子
 */
public interface Activity {

    /**
     * 租房接口
     */
    void rentHouse();
}
