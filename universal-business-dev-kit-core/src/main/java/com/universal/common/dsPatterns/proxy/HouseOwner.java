package com.universal.common.dsPatterns.proxy;

/**
 * 房东
 */
public class HouseOwner implements Activity{

    /**
     * 实现租房方法
     */
    @Override
    public void rentHouse() {
        System.out.println("房东成功出租了房子...");
    }
}
