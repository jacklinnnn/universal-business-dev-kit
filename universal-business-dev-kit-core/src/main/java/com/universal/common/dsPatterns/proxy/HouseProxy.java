package com.universal.common.dsPatterns.proxy;

/**
 * 中介代理类
 *
 * 一般情况下我们不能直接联系到房东，所以需要提供一个代理类，即中介类
 */
public class HouseProxy implements Activity{

    private HouseOwner houseOwner = new HouseOwner();

    @Override
    public void rentHouse() {
        System.out.println("中介收取代理费，帮助房东出租房子...");
        houseOwner.rentHouse();
    }

    public static void main(String[] args) {
        HouseProxy proxy = new HouseProxy();
        proxy.rentHouse();
    }
}
