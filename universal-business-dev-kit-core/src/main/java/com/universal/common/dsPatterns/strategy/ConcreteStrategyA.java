package com.universal.common.dsPatterns.strategy;

import com.universal.common.dsPatterns.enums.StrategySelectorEnum;

public class ConcreteStrategyA implements Strategy {

    @Override
    public String strategy() {
        return StrategySelectorEnum.strategyA.getStrategy();
    }

    @Override
    public void algorithm() {
        System.out.println("process with strategyA...");
    }
}
