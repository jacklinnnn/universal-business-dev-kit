package com.universal.common.dsPatterns.strategy;

import com.universal.common.dsPatterns.enums.StrategySelectorEnum;

public class ConcreteStrategyB implements Strategy {

    @Override
    public String strategy() {
        return StrategySelectorEnum.strategyB.getStrategy();
    }

    @Override
    public void algorithm() {
        System.out.println("process with strategyB...");
    }
}
