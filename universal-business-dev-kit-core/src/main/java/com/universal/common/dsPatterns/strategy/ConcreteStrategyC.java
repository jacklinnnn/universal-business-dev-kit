package com.universal.common.dsPatterns.strategy;

import com.universal.common.dsPatterns.enums.StrategySelectorEnum;

public class ConcreteStrategyC implements Strategy {

    @Override
    public String strategy() {
        return StrategySelectorEnum.strategyC.getStrategy();
    }

    @Override
    public void algorithm() {
        System.out.println("process with strategyC...");
    }
}
