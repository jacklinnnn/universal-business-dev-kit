package com.universal.common.dsPatterns.strategy;

public interface Strategy {

    /**
     * 采用策略
     */
    String strategy();

    /**
     * 计算方法
     */
    void algorithm();
}
