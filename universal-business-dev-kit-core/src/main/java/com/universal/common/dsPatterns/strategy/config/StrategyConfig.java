package com.universal.common.dsPatterns.strategy.config;

import com.universal.common.dsPatterns.strategy.ConcreteStrategyA;
import com.universal.common.dsPatterns.strategy.ConcreteStrategyB;
import com.universal.common.dsPatterns.strategy.ConcreteStrategyC;
import com.universal.common.dsPatterns.strategy.Strategy;
import com.universal.common.dsPatterns.strategy.runner.StrategyRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Configuration
public class StrategyConfig {

    @Bean
    public StrategyRunner strategyRunner() {
        List<Strategy> strategies = Arrays.asList(new ConcreteStrategyA(), new ConcreteStrategyB(), new ConcreteStrategyC());
        Map<String, Strategy> strategyMap = strategies.stream().collect(Collectors.toMap(Strategy::strategy, s -> s));
        return strategy -> strategyMap.get(strategy).algorithm();
    }
}
