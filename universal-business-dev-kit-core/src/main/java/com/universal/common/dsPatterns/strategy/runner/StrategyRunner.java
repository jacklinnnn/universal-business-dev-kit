package com.universal.common.dsPatterns.strategy.runner;

public interface StrategyRunner {

    void execute(String strategy);
}
