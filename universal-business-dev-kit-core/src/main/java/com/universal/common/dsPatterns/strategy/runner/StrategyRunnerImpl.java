package com.universal.common.dsPatterns.strategy.runner;

import com.google.common.collect.Maps;
import com.universal.common.dsPatterns.strategy.ConcreteStrategyA;
import com.universal.common.dsPatterns.strategy.ConcreteStrategyB;
import com.universal.common.dsPatterns.strategy.ConcreteStrategyC;
import com.universal.common.dsPatterns.strategy.Strategy;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class StrategyRunnerImpl implements StrategyRunner {

    private static final List<Strategy> STRATEGIES = Arrays.asList(new ConcreteStrategyA(), new ConcreteStrategyB(), new ConcreteStrategyC());
    private static Map<String, Strategy> STRATEGY_MAP = Maps.newHashMap();

    static {
        STRATEGY_MAP = STRATEGIES.stream().collect(Collectors.toMap(Strategy::strategy, s -> s));
    }

    @Override
    public void execute(String strategy) {
        STRATEGY_MAP.get(strategy).algorithm();
    }
}
