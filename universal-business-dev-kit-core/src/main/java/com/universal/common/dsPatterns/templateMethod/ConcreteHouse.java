package com.universal.common.dsPatterns.templateMethod;

/**
 * 混泥土房屋
 */
public class ConcreteHouse extends HouseTemplate {
    @Override
    public void buildPillars() {
        System.out.println("Building Pillars With Concrete coating...");
    }

    @Override
    public void buildWalls() {
        System.out.println("Building Concrete Walls...");

    }
}
