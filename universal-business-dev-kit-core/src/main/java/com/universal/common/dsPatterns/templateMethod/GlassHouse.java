package com.universal.common.dsPatterns.templateMethod;

/**
 * 玻璃房
 */
public class GlassHouse extends HouseTemplate {

    @Override
    public void buildPillars() {
        System.out.println("Building Pillars With Glass coating...");
    }

    @Override
    public void buildWalls() {
        System.out.println("Building Glass Walls...");

    }
}
