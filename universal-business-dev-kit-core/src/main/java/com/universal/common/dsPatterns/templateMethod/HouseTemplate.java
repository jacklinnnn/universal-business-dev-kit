package com.universal.common.dsPatterns.templateMethod;

public abstract class HouseTemplate {

    /**
     * buildHouse()是模板方法，定义个执行几个步骤的执行顺序
     *
     * template method, final so subclasses can't override final修饰，子类不能重写
     */
    public final void buildHouse(){
        //建造地基
        buildFoundation();
        //建造柱子
        buildPillars();
        //建造墙壁
        buildWalls();
        //建造窗户
        buildWindows();
        System.out.println("House is built successfully");
    }

    private void buildFoundation() {
        System.out.println("Building foundation with cement, iron rods and sand");
    }

    /**
     * methods to be implemented by subclasses
     */
    public abstract void buildPillars();
    public abstract void buildWalls();

    /**
     * default implementation
     */
    private void buildWindows() {
        System.out.println("Building Glass Windows");
    }


}
