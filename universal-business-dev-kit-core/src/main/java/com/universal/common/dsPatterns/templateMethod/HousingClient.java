package com.universal.common.dsPatterns.templateMethod;

public class HousingClient {

    public static void main(String[] args) {
        HouseTemplate houseBuilder = new WoodenHouse();
        houseBuilder.buildHouse();

        System.out.println("--------------");

        houseBuilder = new GlassHouse();
        houseBuilder.buildHouse();

        System.out.println("--------------");
        houseBuilder = new ConcreteHouse();
        houseBuilder.buildHouse();

    }
}
