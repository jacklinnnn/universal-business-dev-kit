package com.universal.common.dsPatterns.templateMethod;

/**
 * 木房
 */
public class WoodenHouse extends HouseTemplate {
    @Override
    public void buildPillars() {
        System.out.println("Building Pillars With Wood coating...");
    }

    @Override
    public void buildWalls() {
        System.out.println("Building Wooden Walls...");
    }
}
