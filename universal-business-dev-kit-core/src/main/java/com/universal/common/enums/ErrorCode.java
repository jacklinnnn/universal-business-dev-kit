package com.universal.common.enums;

import lombok.Getter;

@Getter
public enum ErrorCode {

    ERR_20000("20000", "系统内部异常", "alert"),
    ERR_20001("20001", "请求参数不合法", "normal"),
    ERR_20002("20002", "用户认证失败", "normal"),
    ERR_20003("20003", "用户认证失败", "normal"),
    ERR_20004("20004", "签名错误", "normal"),
    ERR_20005("20005", "限流异常", "alert");

    private String code;
    private String description;
    private String outputType;


    ErrorCode(String code, String description, String outputType) {
        this.code = code;
        this.description = description;
        this.outputType = outputType;
    }

    public static ErrorCode parse(String code) {
        for (ErrorCode errorCode : ErrorCode.values()) {
            if (errorCode.getCode().equals(code)) {
                return errorCode;
            }
        }
        return null;
    }
}
