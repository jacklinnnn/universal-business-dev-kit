package com.universal.common.enums;

import lombok.Getter;

/**
 * 是否枚举
 *
 * @author austin
 * @since 2022/11/24 10:06
 */
@Getter
public enum Whether {

    YES(1, "是"),
    NO(0, "否"),
    ;

    private final Integer code;
    private final String desc;

    Whether(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }
}
