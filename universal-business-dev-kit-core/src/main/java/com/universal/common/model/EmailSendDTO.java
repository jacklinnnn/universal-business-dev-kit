package com.universal.common.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotEmpty;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EmailSendDTO extends SendDTO {

    @NotEmpty
    private String email;

    private List<MultipartFile> files;

    private String executor;

}
