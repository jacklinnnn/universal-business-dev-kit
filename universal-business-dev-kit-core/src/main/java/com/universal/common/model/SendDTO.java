package com.universal.common.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SendDTO {

    private String content;
    private Object additionData;
    private String templateCode;
    private String templateLg;
    private String subject;
    private String createBy;
    private String targetUserId;
    private String version = "1.0.0";
}
