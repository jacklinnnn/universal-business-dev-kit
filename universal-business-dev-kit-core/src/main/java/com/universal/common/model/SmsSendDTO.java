package com.universal.common.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SmsSendDTO extends SendDTO{

    private String callPrefix;

    @NotEmpty
    private String mobile;
}
