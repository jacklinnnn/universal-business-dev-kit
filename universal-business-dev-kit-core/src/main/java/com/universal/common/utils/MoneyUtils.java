package com.universal.common.utils;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.toolkit.Assert;

import java.math.BigDecimal;

/**
 * 金钱计算工具类
 *
 * @author austin
 * @since 2022/12/17 10:22
 */
public class MoneyUtils {

    /**
     * 将单位为元的金额转换为单位为分
     *
     * @param yuan 单位为元的字符型值
     * @return
     */
    public static long yuan2Fen(BigDecimal yuan) {
        long value = 0;
        if (ObjectUtil.isEmpty(yuan)) {
            return value;
        }
        try {
            BigDecimal var2 = new BigDecimal(100);
            BigDecimal var3 = yuan.multiply(var2);
            value = Long.parseLong((var3.stripTrailingZeros().toPlainString()));
        } catch (Exception e) {
            throw new IllegalArgumentException(String.format("非法金额[%s]", yuan));
        }

        Assert.isTrue(value >= 0, String.format("非法金额[%s]", yuan));
        return value;
    }

    /**
     * 将单位为分的金额转换为单位为元
     *
     * @param fen 单位为分的字符型值
     * @return
     */
    public static BigDecimal fen2YuanDecimal(long fen) {
        BigDecimal var1 = new BigDecimal(fen);
        BigDecimal var2 = new BigDecimal(100);
        BigDecimal var3 = var1.divide(var2);
        return var3;
    }
}
