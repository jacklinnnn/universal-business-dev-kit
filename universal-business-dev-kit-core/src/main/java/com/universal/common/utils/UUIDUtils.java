package com.universal.common.utils;

import java.util.UUID;

/**
 * UUIDUtils
 *
 * @author: austin
 * @since: 2023/1/31 13:22
 */
public class UUIDUtils {

    public static String uuid() {
        return UUID.randomUUID().toString().replace("-", "");
    }

    /**
     * Test methods
     */
    public static void main(String[] args) {
        for (int i = 0; i < 100; i++) {
            System.out.println(UUIDUtils.uuid());
        }
    }

}
