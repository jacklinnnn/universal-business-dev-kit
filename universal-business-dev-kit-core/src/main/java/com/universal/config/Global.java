package com.universal.config;

import com.ctrip.framework.apollo.Config;
import com.ctrip.framework.apollo.ConfigService;
import com.google.common.collect.Maps;
import com.universal.common.utils.PropertiesLoader;
import org.apache.commons.lang3.ObjectUtils;

import java.util.Map;

/**
 * Apollo Global Configuration
 *
 * @author: austin
 * @since: 2023/1/6 15:51
 */
public class Global {

    private static Map<String, String> map = Maps.newHashMap();
    private static PropertiesLoader propertiesLoader = new PropertiesLoader(new String[]{"setting.properties"});
    private static Boolean isApollo;

    public Global() {
    }

    public static String getConfig(String key) {
        if (isApollo) {
            String value = getFromApollo(key);
            return value == null ? (String) ObjectUtils.defaultIfNull(getFromConfig(key), "") : value;
        } else {
            return getFromConfig(key);
        }
    }

    public static String getFromApollo(String key) {
        Config config = ConfigService.getAppConfig();
        return config.getProperty(key, (String) null);
    }

    public static String getFromConfig(String key) {
        String value = (String) map.get(key);
        if (value == null) {
            value = propertiesLoader.getProperty(key);
            map.put(key, value);
        }

        return value;
    }

    public static boolean getSendMsgValve() {
        return Boolean.parseBoolean(getConfig("send.msg.valve"));
    }

    public static String getMyDomain() {
        return getConfig("my.domain");
    }

    public static String getSendMsgUrl() {
        return getConfig("send.msg.url");
    }

    public static String getSendMsgPrivateKey() {
        return getConfig("send.msg.privateKey");
    }

    public static String getSendMsgPublicKey() {
        return getConfig("send.msg.publicKey");
    }

    static {
        try {
            isApollo = Class.forName("com.ctrip.framework.apollo.Config") != null;
        } catch (Throwable var1) {
            isApollo = false;
        }
    }
}
