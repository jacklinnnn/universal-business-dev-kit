package com.universal.converter.mapstruct;

import com.universal.entity.UniversalUser;
import com.universal.vo.UniversalUserVO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * BaseDTO -> BaseEntity
 *
 * @author: austin
 * @since: 2023/2/25 22:27
 */
@Mapper
public interface UniversalUserConverter extends MapStructConverter<UniversalUser, UniversalUserVO> {

    UniversalUserConverter INSTANCE = Mappers.getMapper(UniversalUserConverter.class);

    @Override
    UniversalUserVO sourceToTarget(UniversalUser user);

}
