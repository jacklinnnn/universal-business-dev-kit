package com.universal.converter.spring;

import com.universal.entity.UniversalUser;
import com.universal.common.utils.ConverterUtils;
import com.universal.vo.UniversalUserVO;

import java.util.List;

/**
 * UniversalUser -> UniversalUserVO
 *
 * @author: austin
 * @date: 2022/11/7 22:19
 */
public class UniversalUserConverter {

    public static UniversalUserVO toVo(UniversalUser user) {
        UniversalUserVO universalUserVO = ConverterUtils.toVo(user, UniversalUserVO.class);
        return universalUserVO;
    }

    public static List<UniversalUserVO> toVo(List<UniversalUser> benefits) {
        return ConverterUtils.toVo(benefits, (UniversalUser user) -> toVo(user));
    }

    public static void main(String[] args) {
        UniversalUser user = new UniversalUser();
        user.setId(0L);
        user.setName("111");
        user.setPassword("111");
        user.setAge("111");
        user.setAddress("111");
        user.setMobile("222");
        user.setEmail("222");
        user.setBirthday("222");
        UniversalUserVO userVO = toVo(user);
        System.out.println("userVO: " + userVO);

    }
}
