package com.universal.exception;

/**
 * @author austin
 */
public class GlobalBusinessException extends RuntimeException {

    private Integer code;

    private String message;

    public GlobalBusinessException(String message) {
        this(500, message);
    }

    public GlobalBusinessException(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
