package com.universal.exception;

/**
 * 参数不合法异常
 *
 * @author: austin
 * @since: 2022/12/20 20:16
 */
public class InvalidParamException extends RuntimeException {

    public InvalidParamException(String message) {
        super(message);
    }
}
