package com.universal.handler;

import com.universal.common.model.MessageResult;

import java.util.Map;

/**
 * 发送消息/Email
 *
 * @author austin
 * @since 2022/12/20 19:20
 */
public interface IMessageHandler {

    MessageResult sendSms(String callPrefix, String mobile, String actionType, String content);

    MessageResult sendSms(String callPrefix, String mobile, String actionType, String content, String targetUserId);

    MessageResult sendSmsWithTemplate(String callPrefix, String mobile, String actionType, String templateCode, String templateLg, Map<String, Object> additionData);

    MessageResult sendSmsWithTemplate(String callPrefix, String mobile, String actionType, String templateCode, String templateLg, Map<String, Object> additionData, String targetUserId);

    MessageResult sendEmail(String email, String subject, String content);

    MessageResult sendEmail(String email, String subject, String content, String targetUserId);

    MessageResult sendEmailWithTemplate(String email, String subject, String templateCode, String templateLg);

    MessageResult sendEmailWithTemplate(String email, String subject, String templateCode, String templateLg, Map<String, Object> additionData);

    MessageResult sendEmailWithTemplate(String email, String subject, String templateCode, String templateLg, Map<String, Object> additionData, String targetUserId);

    MessageResult sendEmailWithAttachment(String email, String subject, String content, String attachmentName, byte[] bytes);

    MessageResult sendEmailWithAttachment(String email, String subject, String content, String targetUserId, String attachmentName, byte[] bytes);

    MessageResult sendEmailWithTemplateAndAttachment(String email, String subject, String templateCode, String templateLg, String attachmentName, byte[] bytes);

    MessageResult sendEmailWithTemplateAndAttachment(String email, String subject, String templateCode, String templateLg, Map<String, Object> additionData, String attachmentName, byte[] bytes);

    MessageResult sendEmailWithTemplateAndAttachment(String email, String subject, String templateCode, String templateLg, Map<String, Object> additionData, String targetUserId, String attachmentName, byte[] bytes);

}
