package com.universal.handler.impl;

import com.universal.common.model.MessageResult;
import com.universal.handler.IMessageHandler;
import com.universal.service.IEmailService;
import com.universal.service.ISmsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class GlobalSmsHandlerImpl implements IMessageHandler {

    @Autowired
    private ISmsService smsService;
    @Autowired
    private IEmailService emailService;

    @Override
    public MessageResult sendSms(String callPrefix, String mobile, String actionType, String content) {
        return null;
    }

    @Override
    public MessageResult sendSms(String callPrefix, String mobile, String actionType, String content, String targetUserId) {
        return null;
    }

    @Override
    public MessageResult sendSmsWithTemplate(String callPrefix, String mobile, String actionType, String templateCode, String templateLg, Map<String, Object> additionData) {
        return null;
    }

    @Override
    public MessageResult sendSmsWithTemplate(String callPrefix, String mobile, String actionType, String templateCode, String templateLg, Map<String, Object> additionData, String targetUserId) {
        return null;
    }

    @Override
    public MessageResult sendEmail(String email, String subject, String content) {
        return null;
    }

    @Override
    public MessageResult sendEmail(String email, String subject, String content, String targetUserId) {
        return null;
    }

    @Override
    public MessageResult sendEmailWithTemplate(String email, String subject, String templateCode, String templateLg) {
        return null;
    }

    @Override
    public MessageResult sendEmailWithTemplate(String email, String subject, String templateCode, String templateLg, Map<String, Object> additionData) {
        return null;
    }

    @Override
    public MessageResult sendEmailWithTemplate(String email, String subject, String templateCode, String templateLg, Map<String, Object> additionData, String targetUserId) {
        return null;
    }

    @Override
    public MessageResult sendEmailWithAttachment(String email, String subject, String content, String attachmentName, byte[] bytes) {
        return null;
    }

    @Override
    public MessageResult sendEmailWithAttachment(String email, String subject, String content, String targetUserId, String attachmentName, byte[] bytes) {
        return null;
    }

    @Override
    public MessageResult sendEmailWithTemplateAndAttachment(String email, String subject, String templateCode, String templateLg, String attachmentName, byte[] bytes) {
        return null;
    }

    @Override
    public MessageResult sendEmailWithTemplateAndAttachment(String email, String subject, String templateCode, String templateLg, Map<String, Object> additionData, String attachmentName, byte[] bytes) {
        return null;
    }

    @Override
    public MessageResult sendEmailWithTemplateAndAttachment(String email, String subject, String templateCode, String templateLg, Map<String, Object> additionData, String targetUserId, String attachmentName, byte[] bytes) {
        return null;
    }
}
