package com.universal.kit;

/**
 * 自定义请求头属性
 *
 * @author austin
 */
public class CustomRequestHeaders {

    /**
     * header properties
     */
    public static final String HEADER_PREFIX = "x-";

    public static final String HEADER_ACCESS_TOKEN = "x-access-token";

    public static final String HEADER_FORWARDED_FOR = "x-forwarded-for";

    public static final String HEADER_TENANT_ID = "x-tenant-id";

}
