package com.universal.service;

import com.universal.base.resp.ApiResult;
import com.universal.common.model.SmsSendDTO;

public interface ISmsService {

    /**
     * 发送短信
     *
     * @param var
     */
    ApiResult<Object> send(SmsSendDTO var);
}
