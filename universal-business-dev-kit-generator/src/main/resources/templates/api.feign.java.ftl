package com.universal.modules.${package.ModuleName}.api.feign;

import com.universal.base.feign.IBaseFeign;
import com.universal.modules.${package.ModuleName}.dto.${entity}DTO;
import org.springframework.cloud.openfeign.FeignClient;

/**
* ${table.comment!} Feign
*
* @author ${author}
* @since ${date}
*/
@FeignClient(name = "global-service-<#if controllerMappingHyphenStyle??>${controllerMappingHyphen}<#else>${table.entityPath}</#if>", url = "${cfg.special}{service.agent.feign.url}",path = "/${package.ModuleName}/<#if controllerMappingHyphenStyle??>${controllerMappingHyphen}<#else>${table.entityPath}</#if>")
public interface I${entity}Feign extends IBaseFeign<${entity}DTO> {

}