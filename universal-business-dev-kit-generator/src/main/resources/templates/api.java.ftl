package com.universal.modules.${package.ModuleName}.api;
import com.universal.modules.${package.ModuleName}.dto.${entity}DTO;
import com.universal.base.feign.IBaseFeign;

/**
* ${table.comment!} API
*
* @author ${author}
* @since ${date}
*/
public interface ${entity}Api extends IBaseFeign<${entity}DTO>{
}