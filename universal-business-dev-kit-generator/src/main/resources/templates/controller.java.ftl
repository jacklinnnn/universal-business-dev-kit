package ${package.Controller};

import com.universal.modules.${package.ModuleName}.entity.${entity};
import com.universal.modules.${package.ModuleName}.service.I${entity}Service;
import com.universal.modules.${package.ModuleName}.api.${entity}Api;
import com.universal.modules.${package.ModuleName}.dto.${entity}DTO;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;

<#if restControllerStyle>
import org.springframework.web.bind.annotation.RestController;
<#else>
import org.springframework.stereotype.Controller;
</#if>
<#if superControllerClassPackage??>
import ${superControllerClassPackage};
</#if>

/**
* ${table.comment!} 前端控制器
* 等效于{@link MediaType＃APPLICATION_JSON_UTF8}的字符串。@deprecated自Spring Framework 5.2起不推荐使用，
* 而推荐使用{@link MediaType#APPLICATION_JSON_VALUE}由于主要的浏览器（例如Chrome）现在已符合规范并正确解释了UTF-8特殊字符 不需要{@code charset = UTF-8}参数。
* @author ${author}
* @since ${date}
*/
<#if restControllerStyle>
    @RestController
<#else>
    @Controller
</#if>
@RequestMapping(value="<#if package.ModuleName??>/${package.ModuleName}</#if>/<#if controllerMappingHyphenStyle??>${controllerMappingHyphen}<#else>${table.entityPath}</#if>", produces = MediaType.APPLICATION_JSON_VALUE)
<#if kotlin>
    class ${table.controllerName}<#if superControllerClass??> : ${superControllerClass}()</#if>
<#else>
    <#if superControllerClass??>
        public class ${table.controllerName} extends ${superControllerClass}
        <I${entity}Service, ${entity},${entity}DTO> implements ${entity}Api {
    <#else>
        public class ${table.controllerName} {
    </#if>

    }
</#if>
