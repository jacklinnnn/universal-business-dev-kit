package com.universal.entity;

import lombok.Data;

/**
 * UniversalBootPost帖子信息
 *
 * @author: austin
 * @date: 2022/10/30 23:25
 */
@Data
public class UniversalPost {

    private Long id;
    private String content;
    private String author;
    private String comment;
    private String category;
    private String column;
    private String recommend;
}
