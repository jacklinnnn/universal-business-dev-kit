package com.universal.entity;

import lombok.Data;

/**
 * UniversalBootUser
 *
 * @author: austin
 * @date: 2022/10/30 23:07
 */
@Data
public class UniversalUser {

    private Long id;
    private String name;
    private String password;
    private Integer age;
    private String address;
    private String mobile;
    private String email;
    private String birthday;

}
