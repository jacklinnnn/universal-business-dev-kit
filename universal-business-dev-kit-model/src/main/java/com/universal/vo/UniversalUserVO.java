package com.universal.vo;

import lombok.Data;

/**
 * @author: austin
 * @date: 2022/11/7 22:20
 */
@Data
public class UniversalUserVO {

    private String name;
    private Integer age;
    private String address;
    private String mobile;
    private String email;
    private String birthday;
}
