package com.universal;
import java.time.LocalDateTime;

import com.universal.base.dto.BaseDTO;
import com.universal.base.entity.BaseEntity;
import com.universal.converter.mapstruct.UniversalUserConverter;
import com.universal.entity.UniversalUser;
import com.universal.vo.UniversalUserVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * MockMvcController
 *
 * @author: austin
 * @date: 2022/11/12 13:36
 */
@Slf4j
@RestController
public class MockMvcController {

    @RequestMapping(value = "/test")
    public String test() {
        log.info("test...");
        return "test";
    }

    @GetMapping(value = "/converter")
    public UniversalUserVO converter() {
        UniversalUser user = new UniversalUser();
        user.setId(0L);
        user.setName("11");
        user.setPassword("111");
        user.setAge("11");
        user.setAddress("11");
        user.setMobile("111");
        user.setEmail("11");
        user.setBirthday("111");
        UniversalUserVO userVO = UniversalUserConverter.INSTANCE.sourceToTarget(user);
        return userVO;

    }
}
