package com.universal.aysncCompose.controller;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.CompletableFuture;

/**
 * @author austin
 * @data 2022/11/7 19:47
 */
@Slf4j
@RestController
@RequestMapping("/api/allOf")
public class AllOfCompose {

    @SneakyThrows
    @GetMapping("/compose/allOf")
    public static void allOf() {
        CompletableFuture<Integer> cf1 = CompletableFuture.supplyAsync(() -> {
            try {
                System.out.println(Thread.currentThread() + " cf1 do something....");
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            return 1;
        });

        CompletableFuture<Integer> cf2 = CompletableFuture.supplyAsync(() -> {
            try {
                System.out.println(Thread.currentThread() + " cf2 do something....");
                int a = 1 / 0;
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            return 2;
        });

        CompletableFuture<Integer> cf3 = CompletableFuture.supplyAsync(() -> {
            try {
                System.out.println(Thread.currentThread() + " cf3 do something....");
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            return 3;
        });

        CompletableFuture<Void> allOfFuture = CompletableFuture.allOf(cf1, cf2, cf3);
        System.out.println("allOfFuture结果-> " + allOfFuture.get());
    }

    @SneakyThrows
    @GetMapping("/compose/anyOf")
    public static void anyOf() {
        CompletableFuture<Integer> cf1 = CompletableFuture.supplyAsync(() -> {
            try {
                System.out.println(Thread.currentThread() + " cf1 do something....");
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            return 1;
        });

        CompletableFuture<Integer> cf2 = CompletableFuture.supplyAsync(() -> {
            try {
                System.out.println(Thread.currentThread() + " cf2 do something....");
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            return 2;
        });

        CompletableFuture<Integer> cf3 = CompletableFuture.supplyAsync(() -> {
            try {
                System.out.println(Thread.currentThread() + " cf3 do something....");
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            return 3;
        });

        CompletableFuture<Object> anyOfFuture = CompletableFuture.anyOf(cf1, cf2, cf3);
        System.out.println("anyOfFuture结果-> " + anyOfFuture.get());
    }

    public static void main(String[] args) {
        allOf();
        anyOf();
    }
}
