package com.universal.aysncCompose.controller;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.CompletableFuture;

/**
 * @author austin
 * @data 2022/11/7 19:34
 */
@Slf4j
@RestController
@RequestMapping("/api/handle")
public class HandleCompose {

    /**
     * handle与whenComplete基本一致，区别在于handle的回调方法有返回值
     */
    @SneakyThrows
    @GetMapping(value = "/compose")
    public static void compose() {
        CompletableFuture<Integer> cf1 = CompletableFuture.supplyAsync(() -> {
            System.out.println("do something....");
            //int a = 1 / 0;
            return 1;
        });

        CompletableFuture<Integer> cf2 = cf1.handle((res, e) -> {
            System.out.println(Thread.currentThread() + " cf2 do something....");
            System.out.println("上个任务结果：" + res);
            System.out.println("上个任务抛出异常：" + e);
            return res + 2;
        });
        System.out.println("cf2结果-> " + cf2.get());
    }

    public static void main(String[] args) {
        compose();
    }


}
