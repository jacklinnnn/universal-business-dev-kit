package com.universal.aysncCompose.controller;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.CompletableFuture;

/**
 * @author austin
 * @data 2022/11/7 18:57
 */
@Slf4j
@RestController
@RequestMapping("/api")
public class ThenAcceptCompose {

    /**
     * thenAccept子任务和父任务公用同一个线程
     */
    @SneakyThrows
    @GetMapping(value = "/compose/thenAccept")
    public static void thenAccept() {
        CompletableFuture<Integer> cf1 = CompletableFuture.supplyAsync(() -> {
            System.out.println(Thread.currentThread() + "cf1 do something....");
            return 1;
        });
        CompletableFuture<Void> cf2 = cf1.thenAccept((res) -> {
            System.out.println(Thread.currentThread() + " cf2 do something...");
        });
        //等待任务1执行完成
        System.out.println("cf1结果->" + cf1.get());
        //等待任务2执行完成
        System.out.println("cf2结果->" + cf2.get());
    }

    /**
     * thenAcceptAsync子任务会新开一个线程执行
     */
    @SneakyThrows
    @GetMapping(value = "/compose/thenAcceptAsync")
    public static void thenAcceptAsync() {
        CompletableFuture<Integer> cf1 = CompletableFuture.supplyAsync(() -> {
            System.out.println("cf1 do something....");
            return 1;
        });
        CompletableFuture<Void> cf2 = cf1.thenAcceptAsync((res) -> {
            System.out.println(Thread.currentThread() + " cf2 do something...");
        });
        //等待任务1执行完成
        System.out.println("cf1结果->" + cf1.get());
        //等待任务2执行完成
        System.out.println("cf2结果->" + cf2.get());
    }

    public static void main(String[] args) {
        thenAccept();
        thenAcceptAsync();
    }
}
