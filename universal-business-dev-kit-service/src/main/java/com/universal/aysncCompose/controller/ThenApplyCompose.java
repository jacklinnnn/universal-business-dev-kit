package com.universal.aysncCompose.controller;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.CompletableFuture;

/**
 * @author austin
 * @data 2022/11/7 18:48
 */
@Slf4j
@RestController
@RequestMapping("/api/thenApply")
public class ThenApplyCompose {

    @SneakyThrows
    @GetMapping(value = "/compose")
    public static void compose() {
        CompletableFuture<Integer> cf1 = CompletableFuture.supplyAsync(() -> {
            System.out.println("cf1 do something....");
            return 1;
        });
        CompletableFuture<Integer> cf2 = cf1.thenApplyAsync((res) -> {
            System.out.println(Thread.currentThread() + " cf2 do something...");
            res += 2;
            return res;
        });
        //等待任务1执行完成
        log.info("cf1 result: {}", cf1.get());
        //等待任务2执行完成
        log.info("cf2 result: {}", cf2.get());

    }

    public static void main(String[] args) {
        compose();
    }
}
