package com.universal.aysncCompose.controller;

/**
 * @author austin
 * @data 2022/11/7 19:38
 */

import com.universal.aysncCompose.asyncTemplate.CompletableFutureTemplate;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/api")
public class ThenCombineWithThreadPool {

    @Autowired
    private CompletableFutureTemplate completableFutureTemplate;

    @SneakyThrows
    @GetMapping("/compose/thenCombineTest")
    public void thenCombineTest() {
        completableFutureTemplate.combine();

    }
}
