package com.universal.aysncCompose.controller;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.CompletableFuture;

/**
 * @author austin
 * @data 2022/11/7 19:14
 */
@Slf4j
@RestController
@RequestMapping("/api")
public class WhenCompleteCompose {

    @SneakyThrows
    @GetMapping(value = "/compose/whenComplete")
    public static void whenComplete() {
        CompletableFuture<Integer> cf1 = CompletableFuture.supplyAsync(() -> {
            System.out.println(Thread.currentThread() + " cf1 do something....");
            //int a = 1 / 0;
            return 1;
        });
        CompletableFuture<Integer> cf2 = cf1.whenComplete((res, e) -> {
            System.out.println("上一个任务的结果: " + res);
            System.out.println("上一个任务抛出的异常: " + e);
            System.out.println(Thread.currentThread() + " cf2 do something...");
        });

        //等待任务1执行完成
        System.out.println("cf1结果->" + cf1.get());
        //等待任务2执行完成
        System.out.println("cf2结果->" + cf2.get());
    }

    public static void main(String[] args) {
        whenComplete();
    }
}
