package com.universal.concurrent.example.atomic;

import com.universal.concurrent.annoations.ThreadSafe;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * AtomicBooleanExample
 *
 * 当前AtomicBooleanExample这段代码演示了如何让某一段代码仅执行一次，不重复执行！
 *
 * @author: austin
 * @date: 2022/11/12 14:53
 */
@Slf4j
@ThreadSafe
public class AtomicBooleanExample {

    /**
     * 同时并发执行的线程数
     **/
    private static int threadPool = 200;

    /**
     * 请求总数
     **/
    private static int clientTotal = 5000;

    private static AtomicBoolean isHappened = new AtomicBoolean(false);

    public static void main(String[] args) throws Exception {
        ExecutorService executorService = Executors.newCachedThreadPool();
        //定义信号量
        final Semaphore semaphore = new Semaphore(threadPool);
        final CountDownLatch countDownLatch = new CountDownLatch(clientTotal);
        //循环5000次，模拟5000次请求
        for (int index = 0; index < clientTotal; index++) {
            executorService.execute(() -> {
                try {
                    semaphore.acquire();
                    test();
                    semaphore.release();
                } catch (Exception e) {
                    log.error("catch exception -> ", e);
                }
                countDownLatch.countDown();
            });
        }
        countDownLatch.await();
        executorService.shutdown();
        log.info("isHappened:{}", isHappened.get());
    }

    private static void test() {
        // 原子性操作，从false -> ture只会执行一次，后面的线程都不会再次执行
        if (isHappened.compareAndSet(false, true)) {
            log.info("execute success!");
        }
    }
}
