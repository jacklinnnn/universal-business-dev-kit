package com.universal.concurrent.example.count;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

/**
 * 计数器
 *
 * @author: austin
 * @date: 2022/11/12 12:23
 */

@Slf4j
public class CounterExample {

    /**
     * 模拟有200个以后发起请求，系统短时间内接收到5000个请求
     **/
    private static int threadPool = 1;
    //private static int threadPool = 200;
    private static int clientTotal = 5000;

    private static int count = 0;

    public static void main(String[] args) {
        ExecutorService executorService = Executors.newCachedThreadPool();
        //定义信号量
        final Semaphore semaphore = new Semaphore(threadPool);
        //循环5000次，模拟5000次请求
        for (int index = 0; index < clientTotal; index++) {
            executorService.execute(() -> {
                try {
                    semaphore.acquire();
                    add();
                    semaphore.release();
                } catch (Exception e) {
                    log.error("catch exception -> ", e);
                }
            });
        }
        executorService.shutdown();
        log.info("count:{}", count);
    }

    private static void add() {
        count++;
    }
}
