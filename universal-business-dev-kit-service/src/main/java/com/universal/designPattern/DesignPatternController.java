package com.universal.designPattern;

import com.universal.common.dsPatterns.factory.PaymentBody;
import com.universal.common.dsPatterns.factory.PaymentStrategyHandler;
import com.universal.common.dsPatterns.strategy.runner.StrategyRunner;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping(value = "/designPatterns")
public class DesignPatternController {

    @Resource
    private StrategyRunner strategyRunner;

    @GetMapping(value = "/algorithm")
    public void algorithm(@RequestParam("strategy") String strategy) {
        strategyRunner.execute(strategy);
    }


    @PostMapping("/pay")
    public Boolean pay(@RequestBody PaymentBody paymentBody){
        return PaymentStrategyHandler.pay(paymentBody);
    }

}
