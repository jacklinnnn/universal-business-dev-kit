package com.universal;

import junit.framework.TestCase;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Unit test for simple App.
 */
public class UniversalBootAppTest extends TestCase {

    public void test() {
        List<String> list = Arrays.asList("code1", "code2", "code5", "code7", "code11");
        List<String> feignList = Arrays.asList("code1", "code2", "code3", "code4", "code5", "code6", "code7", "code8", "code9", "code10", "code11", "code12");
        feignList = feignList.stream().filter(v -> {
            return  !list.contains(v);
        }).collect(Collectors.toList());
        System.out.println(feignList);
    }
}
