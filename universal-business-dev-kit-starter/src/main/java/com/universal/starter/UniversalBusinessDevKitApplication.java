package com.universal.starter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.env.Environment;

import javax.annotation.PostConstruct;

/**
 * UniversalBusinessDevKit启动类
 *
 * @author austin
 */
@SpringBootApplication
@ComponentScan(basePackages = "com.universal")
public class UniversalBusinessDevKitApplication {

    @Autowired
    private Environment env;

    /**
     * SpringBoot Environment获取配置属性
     */
    @PostConstruct
    public void init() {
        System.out.println("Hello World" + ":" + env.getProperty("spring.application.name"));
    }

    public static void main(String[] args) {
        SpringApplication.run(UniversalBusinessDevKitApplication.class, args);
    }

}
