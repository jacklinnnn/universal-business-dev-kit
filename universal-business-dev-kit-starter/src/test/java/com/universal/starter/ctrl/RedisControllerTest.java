package com.universal.starter.ctrl;

import com.google.common.collect.Maps;
import com.universal.constants.CacheExpireConst;
import com.universal.jedis.JedisUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.Map;

/**
 * RedisControllerTest
 *
 * @author: austin
 * @since: 2023/2/2 11:48
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@WebAppConfiguration
public class RedisControllerTest {

    @Test
    public void test() {
        Map<String, String> map = Maps.newHashMap();
        map.put("foo", "bar");
        JedisUtils.getInstance().setMap("map", map, CacheExpireConst.INT_10MIN);

        Map<String, String> target = JedisUtils.getInstance().getMap("map");
        System.out.println("target: " + target);
    }
}
